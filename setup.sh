######################################3
#
# This script contains the steps required to install autosubmit cloning the repository
#


# Create virtual environment with python 2.7 ( Right now autosubmit < 4.0 uses python 2. Future releases will work with python3)
virtualenv --python=python2 venv

# Activate environment
source venv/bin/activate

# Install autosubmit
pip install autosubmit

# One hidden dependency
pip install requests

# Autosubmit configure
autosubmit configure

# Autosubmit install
autosubmit install
