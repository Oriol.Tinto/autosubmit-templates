# Get workdir from autosubmit
WORKDIR=%HPCROOTDIR%

# If folder does not exist create it
if [ ! -d ${WORKDIR} ] ; then
	mkdir -p ${WORKDIR}
fi

# Go to working directory
cd ${WORKDIR}

# If the repository its already there, remove it
rm -rf icon-examples

# Clone the repository
git clone git@gitlab.physik.uni-muenchen.de:w2w/icon-examples.git

