# Go to autosubmit experiment folder :
WORKDIR=%HPCROOTDIR%


cd ${WORKDIR}
MEMBER=%MEMBER%

# In case there's no results folder, create it:

if [ ! -d results ] ; then
	mkdir results
fi

# Copy some file of interest
file_of_interest=${MEMBER}/icon-examples/02-real-from-ideal/02-real-run/init_DOM01_ML_0001.nc

cp ${file_of_interest} results/results_from_${MEMBER}.nc

