# Go to autosubmit experiment folder:
WORKDIR=%HPCROOTDIR%

cd ${WORKDIR}


# Create Member folder and copy repository there
MEMBER=%MEMBER%
mkdir ${MEMBER}
cp -r icon-examples ${MEMBER}
cd ${MEMBER}

# Set proper environment
set_environment() {
	source /etc/profile.d/modules.sh
        module purge
        # Working version
        module load icon-nwp/2.5.0-gcc-9 python/3.7-2019.10 enstools/2019.12.a1-py3

        # Not working version
        # module load icon-nwp/2.5.0-gcc-9 python enstools
}
set_environment

# Go to example directory
cd icon-examples/02-real-from-ideal

# Launch phase 1
set -xuve
export MPIEXEC=mpirun
export MPIFLAGS="-n 8"
./1_run_ireadlized-run.sh
