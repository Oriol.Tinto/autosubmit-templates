# Go to autosubmit experiment folder corresponding to current member:
WORKDIR=%HPCROOTDIR%
MEMBER=%MEMBER%
cd ${WORKDIR}/${MEMBER}


# Set proper environment
set_environment() {
	source /etc/profile.d/modules.sh
        module purge
        # Working version
        module load icon-nwp/2.5.0-gcc-9 python/3.7-2019.10 enstools/2019.12.a1-py3

        # Not working version
        # module load icon-nwp/2.5.0-gcc-9 python enstools
}
set_environment

# Disable HDF5 version check
export HDF5_DISABLE_VERSION_CHECK=2

# Go to example directory
cd icon-examples/02-real-from-ideal

# Launch phase 2
export MPIEXEC=mpirun
export MPIFLAGS="-n 8"
./2_run_real-run.sh

