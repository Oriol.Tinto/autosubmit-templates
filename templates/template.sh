# Autosubmit will automatically replace some variables with information about the JOB.
# The full list of variables that can be used in each job can be found in the documentation at:
# https://autosubmit.readthedocs.io/en/latest/variables.html
#

# Here some examples:

echo "JOBNAME: %JOBNAME%"
echo "TASKTYPE: %TASKTYPE%"
echo "MEMBER: %MEMBER%"
echo "CHUNK: %CHUNK%"
