# autosubmit-templates

Dummy project to use with autosubmit


## Setup

The script setup.py installs autosubmit in a virtual environment and initializes autosubmit.
It will create a folder called autosubmit in the home directory.

## Create and run an autosubmit experiment

Once installed, to use autosubmit we have to activate the virtual environment.
Check that autosubmit is usable with:
```
autosubmit -h

```

We can find how autosubmit works in the [documentation](https://autosubmit.readthedocs.io/en/latest/tutorial.html).

However, we will reproduce some of the steps here:

To create a new experiment, we first need an experiment id. We can obtain that using the command:

```
autosubmit expid -H HPCname -d Description
```

In our case it could be something like:
```
autosubmit expid -H LMU -d "Running a first test example."
```


If everything works well we will obtain an output that looks like:

```
The new experiment "a000" has been registered.
Copying config files...
Experiment registered successfully

```

In this case **a000** would be our experiment id.

Besides the expid itself, autosubmit will also create a new folder in the autosubmit folder. 
> :bulb: If we don't change the configuration the autosubmit folder will be `~/autosubmit`

In this case it will be `~/autosubmit/a000/`.

If we take a look in this folder we can see that there are few folders:
```
conf
plot
pkl
tmp
```

We are interested in the files inside **conf**.

> :bulb: Look [here](https://autosubmit.readthedocs.io/en/latest/tutorial.html#second-step-experiment-configuration) for a more detailed explanation on what's in each file. 

As we want to test how to get all the required information from a git project, we will modify the file **expdef_xxxx.conf"** to point to this very same repository. To do so we will go to section **[project]**, set `PROJCET_TYPE = git` and under the **[git]** section we will set `PROJECT_ORIGIN = git@gitlab.physik.uni-muenchen.de:Oriol.Tinto/autosubmit-templates.git`.

Also, we need to modify the file **platforms_xxxx.conf** to add information about the platforms that we want to use.

In the case of using the LMU cluster our file would need the following content:
```
[LMU]
TYPE = SLURM
HOST = workstation
PROJECT = ls-craig
USER = oriol.tinto
SCRATCH_DIR = /project/meteo-scratch/Oriol.Tinto/tmp/autosubmit
```




```
autosubmit create a000 -np
```

> :warning: At the moment of writting this documentation autosubmit has a problem using Graphviz 2.40, to bypass this we use the flag **-np** to skip plot generation.

# Troubleshooting

## Scheduler not available
In our system sbatch is not available when using non-interactive shells. Like the one that you get with autosubmit.
To solve that, we have to add this to .bashrc:

```bash
PATH=${PATH}:/opt/slurm/bin/
```

## Current host is not allowed
In case of seeing this error:
```
 [CRITICAL] The current host is not allowed to run Autosubmit [eCode=7004]
```

We need to modify the file **.autosubmitrc** that can be found in our home directory and just comment the line with **whitelist**:

```diff
-whitelist =  localhost # Add your machine names
+# whitelist =  localhost # Add your machine names
```
